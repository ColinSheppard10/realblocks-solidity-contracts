pragma solidity 0.4.24;

import './RegulatedToken.sol';
import './ServiceRegistry.sol';

contract RealBlocksToken is RegulatedToken {
  /**
   * @notice Constructor, creates the r-token
   * param _registry: address of ser-reg which tells token when to find the reg-ser
   * param _name: token's name
   * param _symbol: token's symbol
   */
  function RealBlocksToken(ServiceRegistry _registry, string _name, string _symbol) public
    RegulatedToken( _registry,  _name,  _symbol)
  {
  }
 
}
